# pop-icon-theme

Pop OS icon theme

https://github.com/pop-os/icon-theme

https://apt-origin.pop-os.org/release/pool/jammy/icon-theme/ (version here)

<br><br>
How to clone this repository:
```
git clone https://gitlab.com/azul4/content/icons-and-themes/pop-icon-theme.git
```

